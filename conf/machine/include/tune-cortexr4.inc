# valid options for -march: `armv7', `armv7-r'
TARGET_CC_ARCH = "-march=armv7-r -mtune=cortex-r4 -mfpu=vfp -mfloat-abi=softfp"
FEED_ARCH = "armv7"
PACKAGE_ARCH = "armv7"
