require xorg-font-common.inc

DESCRIPTION = "X font utils."

DEPENDS = "encodings"
RDEPENDS = "mkfontdir mkfontscale encodings"

PR = "r1"
PE = "1"
