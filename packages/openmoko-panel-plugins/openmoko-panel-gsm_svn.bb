DESCRIPTION = "Shows the GSM / GPRS status in the Openmoko panel"
DEPENDS = "libgsmd libnotify"
PV = "0.1.0+svn${SVNREV}"
PR = "r1"

inherit openmoko-panel-plugin
