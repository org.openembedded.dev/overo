DESCRIPTION = "Exhibit is the ETK picture viewer"
LICENSE = "MIT BSD"
DEPENDS = "evas ecore epsilon edje eet etk efreet"
PV = "0.1.1+cvs${SRCDATE}"
PR = "r0"

SRCNAME = "exhibit"

inherit e
