DESCRIPTION = "Python Examples for the Enlightenment Foundation Libraries"
LICENSE = "MIT"
SECTION = "devel/python"
RDEPENDS = "\
  task-python-efl \
  python-ecore-examples \
  python-emotion-examples \
  python-edje-examples \
  python-epsilon-examples \
  python-math python-textutils \
"
PR = "ml3"

ALLOW_EMPTY = "1"
