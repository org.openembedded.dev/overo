require xorg-lib-common.inc

DESCRIPTION = "X11 keyboard file manipulation library"
LICENSE= "GPL"
DEPENDS += "virtual/libx11 kbproto"
PE = "1"
