require linux-omap.inc

# uncomment the below to get the latest and greatest and avoid a full reparse
# or override in it local.conf like this: DAVINCI_SRCREV_pn-linux-davinci = "${@bb.fetch.get_srcrev(d)}"
#DAVINCI_SRCREV = "${@bb.fetch.get_srcrev(d)}"
DAVINCI_SRCREV ?= "713dc561fa98efb6cbac3f641c43d30ce8b840dd"
SRCREV = "${DAVINCI_SRCREV}"

PV = "2.6.25"
#PV = "2.6.25+2.6.26-rc0+git${SRCREV}"
PR = "r1"

COMPATIBLE_MACHINE = "(davinci-dvevm|davinci-sffsdr)"

SRC_URI = "git://source.mvista.com/git/linux-davinci-2.6.git;protocol=git \
           file://defconfig"

SRC_URI_append_davinci-sffsdr = " file://sffsdr.patch;patch=1"

S = "${WORKDIR}/git"
